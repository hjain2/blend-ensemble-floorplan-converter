import bpy
import json
import numpy as np
import os, sys
import cv2

print(sys.argv[-1])
sys.path.insert(0, sys.argv[-2])

from geometry_2d import *
from floorplan_classes import *

def meters_to_feet(val):
	return val*3.28084*scale

def remove_duplicates(items):
	# removes duplicate items from the list of items.

	new_items = []
	for item_no, item in enumerate(items):
		flag = 0
		if  not (item_no == len(items)-1):
			for item_comp in items[item_no+1:]:
				if np.array_equal(np.array(item), np.array(item_comp)):
					flag = 1
					break
		if not flag:
			new_items.append(item)
	return new_items


def get_corners(obj_type):
	
	"""
		Get the set of 2D locations for each object type.
		
		Input: 
			obj_type: one of  ["room","window","door"]
		Output:
			corners:
				- N-length list for N rooms/windows/doors. 
				- each item in list is (Vi X 2) array of Vi corners of the the ith room/window/door.   
	"""

	obj_names = [obj.name for obj in bpy.data.objects if obj.name.startswith(obj_type)]
	corners = []
	for obj_name in obj_names:
		obj = bpy.data.objects[obj_name]
		obj_offset = [meters_to_feet(x) for x in obj.location]
		curve_points = []
		for point in obj.data.splines[0].points:
			#curve_points.append([meters_to_feet(x) for x in np.array(point.co)[:3]]+np.array(obj_offset))

			curve_points.append([meters_to_feet(x) for x in np.array(obj.matrix_world @ point.co)[:3]]+np.array(obj_offset))
			#curve_points.append(list(np.array(point.co)[:3]+np.array(obj_offset)))
		corners.append(([1,-1]*np.array(curve_points)[:,:2]).tolist())
	corners = remove_duplicates(corners)
	return corners


def assign_room(endpoints, rooms, obj):
	"""
		Assigns the window/door object to the respectful rooms.

		Input:
			endpoints:	NX2 array of the N=2 endpoints of the object.
			rooms:	List of FloorPlan.room objects.
			obj: FloorPlan.Window / FloorPlan.Door object with some assigned default values.
		Output:
			rooms: List of updated FloorPlan.room objects. 
	"""
	p1, p2 = endpoints
	midpoint = Point((p1.x+p2.x)/2, (p1.y+p2.y)/2)
	obj_length = euclidean_dist(p1, p2)
	min_dist = 9999
	# iterate over all rooms and their walls.
	for room in rooms.rooms:
		for corner_no, corner in enumerate(room.corners):
			
			# get 2d wall line
			wall_endpoint1 = corner.point
			if corner_no == len(room.corners) - 1:
				wall_endpoint2 = room.corners[0].point
			else:
				wall_endpoint2 = room.corners[corner_no+1].point
			wall_line = Line.from_two_points(wall_endpoint1, wall_endpoint2)

			# find where item's midpoint cuts the wall 
			wall_normal = wall_line.get_normal_through_point(midpoint)
			#print(wall_line.__dict__, midpoint.__dict__,wall_normal.__dict__)
			normal_intersection_point = point_of_line_intersection(wall_normal, wall_line)
			#print(normal_intersection_point.__dict__)
			# check if the item is outside the wall.
			wall_length = euclidean_dist(wall_endpoint1, wall_endpoint2)
			#print(wall_endpoint1, normal_intersection_point)
			split_length_1 = euclidean_dist(wall_endpoint1, normal_intersection_point)
			split_length_2 = euclidean_dist(wall_endpoint2, normal_intersection_point)
			if split_length_1>wall_length or split_length_2>wall_length:
				# external division
				continue

			# Minimizing iteration check
			item_wall_dist = euclidean_dist(midpoint,normal_intersection_point)
			relative_arch_item_position = split_length_1/wall_length 
			if item_wall_dist<min_dist:
				wall_index = corner_no+1
				if wall_index == len(room.corners):
					wall_index = 0
				obj.assign_values(room.ID,wall_index, relative_arch_item_position, obj_length)
				min_dist = item_wall_dist

	# assign item to corresponding room.
	for room in rooms.rooms:
		if room.ID == obj.room:
			room.architectures.append(obj)
	return rooms

### get scale
global scale
for obj in bpy.data.objects:
	if obj.name.startswith("scale"):
		scale = float(obj.name.split(";")[1])
		break

rooms_corners = get_corners("room")
windows_corners = get_corners("window")
doors_corners = get_corners("door")
shapes_corners = get_corners("shape")

print("Found {} unique rooms".format(len(rooms_corners)))
print("Found {} unique windows".format(len(windows_corners)))
print("Found {} unique doors".format(len(doors_corners)))
print("Found {} unique shapes".format(len(shapes_corners)))


floorplan = FloorPlan()


for room_no, room_corners in enumerate(rooms_corners):
	room = Rooms.Room(room_no)
	for corner_no, room_corner in enumerate(room_corners):
		room.add_corner(corner_no, Point(*room_corner))
	floorplan.rooms.rooms.append(room)


for window_no, window_endpoints in enumerate(windows_corners):
	window = Rooms.Window(window_no)
	floorplan.rooms = assign_room((Point(*window_endpoints[0]),Point(*window_endpoints[1])), floorplan.rooms, window)


for door_no, door_endpoints in enumerate(doors_corners):
	door = Rooms.Door(door_no + len(windows_corners))
	floorplan.rooms = assign_room((Point(*door_endpoints[0]),Point(*door_endpoints[1])), floorplan.rooms, door)

for shape_no, shape_corners in enumerate(shapes_corners):
	symbol = Symbols.Product(shape_no)
	center, dimensions, angle = cv2.minAreaRect(np.array(shape_corners, dtype= np.float32))
	symbol.assign_values(Point(*center), angle*(8/90), dimensions[0], dimensions[1], "rectangle")
	floorplan.symbols.products.append(symbol)

floorplan.write_to_json(sys.argv[-1])

bpy.ops.wm.quit_blender()








"""
with open("63311.json","r") as f:
	json_data = json.load(f)
print(json_data["rooms"]["rooms"])
"""