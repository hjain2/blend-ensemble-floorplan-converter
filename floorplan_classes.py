# Classes that define the major fields in the roomplan jsons.

import json
from geometry_2d import *


#### Classes that are likely used at the frontend.
#### Left empty for an empty floorplan.

class Products:
	def __init__(self, version = "1.0.3", products = []):
		self.version = version
		self.products = products

	def get_json_entry(self):
		json_dict = {var: vars(self)[var] for var in vars(self)}
		return json_dict

class Captures:
	
	class Capture:
		def __init__(self):
			return

	def __init__(self, captures = []):
		self.captures = captures
		return

	def get_json_entry(self):
		return self.captures

class FabricAttributes:
	
	class FabricAttribute:
		def __init__(self):
			return

	def __init__(self, fabricattributes = []):
		self.fabricattributes = fabricattributes

	def get_json_entry(self):
		return self.fabricattributes

class ProductAttributes:

	class ProductAttribute:
		def __init__(self):
			return
 
	def __init__(self, productattributes = []):
		self.productattributes = productattributes

	def get_json_entry(self):
		return self.productattributes

class Shapes:

	class Shape:
		def __init__(self):
			return

	def __init__(self, version = "1.0.3", shapes = []):
		self.version = version
		self.shapes = shapes

	def get_json_entry(self):
		json_dict = {var: vars(self)[var] for var in vars(self)}
		return json_dict


class Thumbnail:

	def __init__(self, val = ""):
		self.val = val
	
	def get_json_entry(self):
		return self.val	


class Version:

	def __init__(self, val = "3.0.1"):
		self.val = val

	def get_json_entry(self):
		return self.val

#### Classes with some default attributes in empty floorplan.

class Camera:
	def __init__(self, x = -420, y= -310, z = 0.5):
		self.x = x
		self.y = y
		self.z = z

	def get_json_entry(self):
		json_dict = {var: vars(self)[var] for var in vars(self)}
		return json_dict

class Moodboards:

	class Moodboard:
		def __init__(self, name = "", tiles = [], layout = 9):
			self.name = name
			self.tiles = tiles
			self.layout = layout

	def __init__(self):
		self.moodboards = [self.Moodboard()]

	def get_json_entry(self):
		json_dict = []
		for item in self.moodboards:
			json_dict.append({var:vars(item)[var] for var in vars(item)})
		return json_dict

class Photoboards:

	class Photoboard:
		def __init__(self):
			self.name = ""
			self.tiles = []
			self.layout = 6

	def __init__(self):
		self.photoboards = [self.Photoboard()]

	def get_json_entry(self):
		json_dict = []
		for item in self.photoboards:
			json_dict.append({var:vars(item)[var] for var in vars(item)})
		return json_dict



#### Classes that are actually parsed from the blend file.

class Rooms:

	class Room:
		
		class Corner:
			def __init__(self, ID, point):
				self.ID = ID
				self.point = point

				# default attributes
				self.wallIsVisible = True
				self.wallIsCruved = False
				self.wallCurve = 90

		def __init__(self, ID):
			self. ID  = ID

			#### these values are added from main() in make_json.py.
			self.corners = []
			self.architectures = []

			# default attributes
			self.floor = "None"
			self.wallColorName = "White"
			self.wallColor = "#FFFFFF"
			self.showdimensions = False

		def add_corner(self, ID, point):
			self.corners.append(self.Corner(ID, point))

	class Architecture_item:
		def __init__(self, ID):
			self.ID = ID

		def assign_values(self, room_id, wall, pos, length ):
			self.room = room_id
			self.wall = wall
			self.pos = pos
			self.length = length

			#default attributes
			self.rot = 0
			self.width = 0
			self.showDimensions = False
			self.offset = 0
			self.direction = 1

	class Window(Architecture_item):
		def __init__(self, ID):
			self.ID = ID
			self.className = "PluginBlueprintArchitectureWindow"

	class Door(Architecture_item):
		def __init__(self, ID):
			self.ID = ID
			self.className = "PluginBlueprintArchitectureFrenchDoor"


	def __init__(self, version = "1.0.7", rooms = []):
		self.version = version
		self.rooms = rooms
				
	def get_json_entry(self):
		json_entry = {"version":self.version, "rooms": []}
		for room in self.rooms:
			json_dict = {"ID": room.ID, \
						"floor": room.floor, \
						"wallColorName": room.wallColorName, \
						"wallColor": room.wallColor, \
						"corners": [], \
						"architectures": []}

			for corner in room.corners:
				corner_dict = {}
				for var in vars(corner):
					var_name, var_val = var, vars(corner)[var]
					
					# special cases
					if var == "point":
						corner_dict["pos"] = {"x": var_val.x, "y": var_val.y}
					

					else:
						corner_dict[var_name] = var_val
				json_dict["corners"].append(corner_dict)
			json_dict["architectures"] = [{var:vars(arch_item)[var] for var in vars(arch_item)} \
											for arch_item in room.architectures]
			json_entry["rooms"].append(json_dict)
		return json_entry


class Symbols:

	class Product:
		def __init__(self, ID):
			self.ID = ID

			#default attributes
			self.className = "PluginProductPlacerProductSymbol"
			self.showDimensions = False

		def assign_values(self, centroid, rot, width, depth, symbol_type):
			self.centroid = centroid
			self.rot = rot
			self.width = width
			self.depth = depth
			self.symbol = "menu-"+symbol_type
			self.title = symbol_type.capitalize()


	def __init__(self, version = "1.0.3", products = []):
		self.version = version
		self.products = products

	def get_json_entry(self):
		json_entry = {}
		json_entry["version"] = self.version
		json_entry["products"] = []
		for product in self.products:
			product_json_entry = {}
			for var in vars(product):
				if var == "centroid":
					product_json_entry["pos"]= {"x": product.centroid.x, "y": product.centroid.y}
				else:
					product_json_entry[var] = vars(product)[var]
			#print(product_json_entry)
			json_entry["products"].append(product_json_entry)
		#print(json_entry)
		return json_entry

"""
Main floor plan class
"""

class FloorPlan:


	def __init__(self):
		self.moodboards = Moodboards()
		self.photoboards = Photoboards()
		self.captures = Captures()
		self.camera = Camera()
		self.fabricAttributes = FabricAttributes()
		self.productAttributes = ProductAttributes()
		self.products = Products()
		self.rooms = Rooms()
		self.shapes = Shapes()
		self.symbols = Symbols()
		self.thumbnail = Thumbnail()
		self.version = Version()


	def write_to_json(self, json_fp):
		json_dict = {var:vars(self)[var].get_json_entry() for var in vars(self)}
		with open(json_fp, "w+") as f:
			json.dump(json_dict, f, indent = 2)
