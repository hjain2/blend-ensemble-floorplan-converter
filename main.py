import os
import subprocess

# Modify filepaths before use!
script_fp = r"/mnt/o/share/hjain/blend-ensemble-floorplan-converter/make_json.py" # fp to the make_json script included in the repo.
blend_fp = r"/mnt/o/share/hjain/blend-ensemble-floorplan-converter/test_data/blends/WE Southlake_FloorPlan.blend" # fp to the blend file.
print(os.path.exists(blend_fp))
blender_executable_fp = r"/home/outward/Blender/2.90.1/blender-2.90.1-linux64/blender" # fp to the blender exectuable
json_out_fp = r"/mnt/o/share/hjain/blend-ensemble-floorplan-converter/test_data/jsons/WE Southlake_FloorPlan.json" # fp to the output floorplan json.

script_dir = os.sep.join(script_fp.split(os.sep)[:-1])
print(script_dir)
output = subprocess.check_output([blender_executable_fp, blend_fp, "-P",  script_fp, "--background", "--", script_dir, json_out_fp])
print(output)