import numpy as np

# %% relevant goemetric functions.
class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y
	def get_array(self):
		return np.array([self.x,self.y])

class Line:

	def __init__(self, a, b, c):
		# initialize using line coefficients.
		self.a =a 
		self.b = b
		self.c = c
		return

	@classmethod
	def from_two_points(self, p1, p2):
		# get the line joining two points p1, p2.
		y_delta = p2.y - p1.y
		x_delta = p2.x - p1.x

		if x_delta == 0:
			# the line is parallel to the x-axis.
			a = 1
			b = 0
			c = -p2.x
		else:
			a = m = y_delta/x_delta
			b = -1
			c = p2.y - (m*p2.x)
		return Line(a, b, c)

	def get_normal_through_point(self, point):
		# returns normal to the line passing through the given point.
		if self.a == 0:
			# the intersection line is parallel to the x-axis.
			a = 1
			b = 0
			c = -point.x
		else:
			a = m = self.b/self.a
			b = -1
			c = point.y - m*point.x
		return Line(a, b, c)


def euclidean_dist(p1, p2):
	# returns euclidean distance between two points.
	return ((p1.x - p2.x)**2 + (p1.y - p2.y)**2)**0.5
	

def point_of_line_intersection(l1,l2):
	# get intersection point of two lines.

	try:
		x = (l1.b*l2.c - l2.b*l1.c)/ (l2.b*l1.a - l1.b*l2.a)
		y = (l1.a*l2.c - l2.a* l1.c)/(l2.a*l1.b - l1.a*l2.b)
		return Point(x, y)
	except:
		# lines are parallel/ anti-parallel.
		return False

	
